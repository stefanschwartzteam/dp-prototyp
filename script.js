 // javascript
var reducedCols;

d3.csv("data.csv", function(d) {
  return {
    auth_id : d.author_id,
    date : new Date(d.author_timestamp * 1000),
    n_add : +d.n_additions,
    n_del : +d.n_deletions,
    // utc : d.commit_utc_offset_hours,
    filename : d.filename,
    message : d.subject,
    hash : d.commit_hash
  };
}).then(function(data) {
  var formatedDate = d3.timeFormat("%d.%m.%Y %H:%M:%S");
  reducedCols = data.map(function(d) {
    return {
      auth_id: d.auth_id,
      date: formatedDate(d.date),
      n_add : +d.n_add,
      n_del : +d.n_del,
      filename: d.filename,
      hash: d.hash
    }
  });
  console.log(reducedCols); // [{"Hello": "world"}, …]
});

// Width and height
var w = 600;
var h = 400;

//creating the basic SVG element
var svg = d3.select("body")
            .append("svg")
            .attr("width", w)
            .attr("height", h);

svg.selectAll("rect")
   .data(reducedCols)
   .enter()
   .append("rect")
   .attr("x", function(d,i) {
    return i * 21;
   })
   .attr("x", 0)
   .attr("y", 0)
   .attr("width", 20)
   .attr("height", 300);

