import csv

with open('data.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    results = []
    skip = True
    for row in readCSV:
        if skip: 
            skip = False
            continue
        results.append({
            'time': row[0],
            'hash': row[1],
            'name': row[3],
            'plus': row[4],
            'del': row[5],
            'author': row[7]
        })


    f = open("data.js", "w")
    f.write("var DATA = ")
    f.write(str(results))
    f.write(";")
    f.close()
