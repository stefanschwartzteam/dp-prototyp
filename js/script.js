 // javascript

// sample code from D3 Tps and trick book - https://bl.ocks.org/d3noob/bdf28027e0ce70bd132edc64f1dd7ea4
// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1100 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// set the ranges
var x = d3.scaleBand()
          .range([0, width])
          .padding(0.1);
var y = d3.scaleLinear()
          .range([height, 0]);

// append the svg object to the body of the page
// append a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

//get data
var reducedCols = [];

d3.csv("data.csv", function(d) {
    return {
      auth_id : +d.author_id,
      date : new Date(d.author_timestamp * 1000),
      n_add : +d.n_additions,
      n_del : +d.n_deletions,
      // utc : d.commit_utc_offset_hours,
      filename : d.filename,
      message : d.subject,
      hash : d.commit_hash
    };
  }).then(function(data) {
  // format the unix date
  var formatedDate = d3.timeFormat("%d.%m.%Y %H:%M:%S");
  var reducedCols = data.map(function(d) {
    // console.log("here");
    return {
      auth_id: d.auth_id,
      date: formatedDate(d.date),
      n_add : +d.n_add,
      n_del : +d.n_del,
      filename: d.filename,
      hash: d.hash
    }
  });

x.domain(data.map(function(d) {
  return d.auth_id;
}))
y.domain([0, d3.max(data, function(d) {
  return d.n_add;
})])

svg.selectAll("rect")
   .data(reducedCols)
   .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) {
        return x(d.auth_id);
      })
      .attr("width", x.bandwidth())
      .attr("height", 30)
      .attr("y", function(d,i) {
        return y(d.n_add);
      })
      .attr("height", function(d) {
        return height - y(d.n_add);
      });

svg.append("g")
   .attr("transform", "translate(0," + height + ")")
   .call(d3.axisBottom(x))

svg.append("g")
   .call(d3.axisLeft(y));

console.log(reducedCols);
});

